/*
 * OS HW4 0110143
 * Data Structure of FAT32
 * redfish.tbc@gmail.com
 */
#include <types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <common.h>
boot_record_t init_boot(int fp){

    boot_record_t boot = {0,0,0,0,0,0,0};
    unsigned char *buffer;
    buffer = (unsigned char*) malloc(60);
   
    // read 60 bytes to buf from 0
    if(read_n_bytes(fp, buffer, 60, 0) == -1){
        perror("-Error: INITIAL BOOT RECORD");
        close(fp);
        free(buffer);
        exit(EXIT_FAILURE);
    }

    int i = 0, ascii[60];
    for (i = 0; i < 60; i++) ascii[i] = buffer[i];

    boot.bytes_per_sector      = ascii[11] | (ascii[12]<<8);
    boot.sectors_per_cluster   = ascii[13];
    boot.reserved_region_ssec  = ascii[14] | (ascii[15]<<8);
    boot.FAT_copies            = ascii[16];
    boot.dir_entries           = ascii[17] | (ascii[18]<<8);
    boot.total_sectors         = ascii[32] | (ascii[33]<<8) 
                                           | (ascii[34]<<16) | (ascii[35]<<24);
    
    boot.sectors_per_FAT       = ascii[36] | (ascii[37]<<8) 
                                           | (ascii[38]<<16) | (ascii[39]<<24);

    return boot;
}

// Data Region 32 bytes
int readEntry(int fp, uint32_t begin){
    unsigned char *buffer;
    buffer = (unsigned char*) malloc(32);
    
    if(read_n_bytes(fp, buffer, 32, begin) == -1){
        perror("-Error: readEntry");
        close(fp);
        free(buffer);
        exit(EXIT_FAILURE);
    }
    
    uint8_t chk = buffer[0];
    printf("chk byte: %d", chk);
    
    if (chk == 0xE5) return 0; // entry is deleted
    else if (chk == 0x00) return -1; // entry is unused
    else return 1;
    
}

// Data Region 32 bytes
int readEntryAssign(int fp, uint32_t begin, dir_table_t *p) {    
    int ret;
    unsigned char *buffer;
    buffer = (unsigned char*) malloc(32);
    
    if(read_n_bytes(fp, buffer, 32, begin) == -1){
        perror("-Error: readEntry");
        close(fp);
        free(buffer);
        exit(EXIT_FAILURE);
    }
    
    uint8_t chk = buffer[0];
    printf("chk byte: %d", chk);
    if (chk == 0xE5) return 0; // entry is deleted
    else if (chk == 0x00) return -1; // entry is unused
    else {
        p->attr = buffer[11];
        switch(p->attr){
            case 0x01: // READ_ONLY
            case 0x02: // HIDDEN
            case 0x04: // SYSTEM
            case 0x08: // VOLUME_ID
            case 0x10: // DIRECTORY=0x10
            case 0x20: // ARCHIVE=0x20
              memcpy(p->name, buffer, 10);                 
              p->clusterNum_HI   =  buffer[20] | (buffer[21]<<8);
              p->clusterNum_LOW  =  buffer[26] | (buffer[27]<<8);
              p->file_size       =  buffer[28] | (buffer[29]<<8)
                                               | (buffer[30]<<16) 
                                               | (buffer[31]<<24);  
              ret = 1;
              break;
                
            case 0x0F: // LFN
                p->seq_num =  buffer[0];
                memcpy(p->str1, buffer+1, 10);
                memcpy(p->str2, buffer+14,12);          
                memcpy(p->str3, buffer+28, 4);
                ret = 2;
                break;

            default:
                ret = -1;
                break;
        }
    }
    return ret;
}


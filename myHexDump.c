/*
 * OS HW4 0110143 
 * Redfish Leo Lee
 * redfish.tbc@gmail.com
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>

#include <common.h>

int main(int argc, char **argv) 
{
    if(argc != 4){
        perror("Usage: ./myHexDump IMAGE START OFFSET");
        exit(-1);
    }

    start = atoi(argv[2]) & 0xFFFFFFF0;
      end = atoi(argv[2]) + atoi(argv[3]);
      end = ((end&0x0F)==0x0F)?end:(end | 0x0F);
     size = end - start + 1;
    
    int fp, start, end, size;
    unsigned char *buffer;
    buffer = (unsigned char*) malloc(size);
    
    if(buffer == NULL){
    	perror("Error allocating memory");
    	exit(-1);
    }
	
    /** OPEN the looper or images using System API */
    /** REMIND: you will need O_NONBLOCK as flag */
    
    // open() return new file descriptor, or -1 when error occurs.
    if ((fp = open(argv[1], O_RDWR, O_NONBLOCK)) == -1){
        perror("Error opening image");
        free(buffer);
        exit(-1);
    }
    
    if(read_n_bytes(fp, buffer, size, start) == -1){
        perror("Error reading image");
        close(fp);
        free(buffer);
        exit(-1);
    }
    
    hexDump(buffer, size, start);  
    
    close(fp); // free the file descriptor
    free(buffer);
    return 0;
}

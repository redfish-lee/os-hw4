#pragma once
#include <types.h>

int read_n_bytes(int fp, void *dest, int offset, int num_bytes);
int write_n_bytes(int fp, void *buf, int offset, int num_bytes);
void hexDump(void *source, int size, int start_offset);
void atoi_array(unsigned char *dest, char **src, int size);

/** my functions  **/
int media_sec_read(int fp, unsigned char *buf, uint32_t begin, uint32_t count);
uint32_t getDRBase(boot_record_t boot);
uint8_t *strcast(const char *str);
int replace(int fp, const char *str, uint32_t sector_begin, uint32_t limit);
int chkUsage(int fp, uint32_t start);

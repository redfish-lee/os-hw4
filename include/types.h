#pragma once
#include <sys/types.h>
#include <stdint.h>

// Reserved Region Sector 0 (Boot Record)
typedef struct boot_record_t {

    uint16_t  bytes_per_sector;        // (11-12) little endian
    uint8_t   sectors_per_cluster;     //    (13)
    uint16_t  reserved_region_ssec;    // (14-15) size in sectors
    uint8_t   FAT_copies;              //    (16) often be 2
    uint16_t  dir_entries;             // (17-18) ???
    uint32_t  total_sectors;           // (32-35) in logical volumn
    uint32_t  sectors_per_FAT;         // (36-39) sectors per FAT
}boot_record_t;

/******************************************************************************
 * Data Region 32 bytes
 * 7 6   5 4~~~~0
 * x Fnd 0 seqNum
 *****************************************************************************/
typedef struct dir_table_t {
    
    /** Directory Enrty **/
    unsigned char *name;        // (00-10) 8 chars + 3 extensions
    uint8_t  attr;              //    (11) directory=0x10, LFN=0x0F

    uint16_t clusterNum_HI;     // (20-21) 2 
    uint16_t clusterNum_LOW;    // (26-27) 2
    uint32_t file_size;         // (28-31) 4
    
    /** LFN type **/
    uint8_t     seq_num;        //    (00) 0xE5 if deleted
    unsigned char *str1;        // (01-10) USC2 (2-byte) 5 chars
    unsigned char *str2;        // (14-25) USC2 (2-byte) 6 chars
    unsigned char *str3;        // (28-31) USC2 (2-byte) 2 chars
    
}dir_table_t;

boot_record_t init_boot(int fp);
int readEntry(int fp, uint32_t begin);
int readEntryAssign(int fp, uint32_t begin, dir_table_t *p);



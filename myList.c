/*
 * OS HW4 0110143
 * Problem 1: List all files in FileSystem
 * Redfish Leo Lee
 * redfish.tbc@gmail.com
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>

#include <common.h>
#include <types.h>

int main(int argc, char **argv) 
{
    if(argc != 2){
        perror("-listAll: input error! try ./listAll /dev/loop0");
        exit(-1);
    }    

    int fp; // file descriptor
    unsigned char *buffer;
    buffer = (unsigned char*) malloc(size);

    /** FILE OPEN **/
    if ((fp = open(argv[1], O_RDWR, O_NONBLOCK)) == -1) {
        perror("Error opening image");
        free(buffer);
        exit(EXIT_FAILURE);
    }
   
    // Boot record initial
    boot_record_t myboot = init_boot(fd);


    // sector number/Target buffer to write n sectors of data into/Number of sectors to read
    // return 1 = succ; 0 = fail
    int media_read(unsigned long sector, unsigned char *buffer, unsigned long sector_count)


    // sector number/Target buffer to write n sectors of data from/Number of sectors to write.
    // return 1 = succ; 0 = fail
    int media_write(unsigned long sector, unsigned char *buffer, unsigned long sector_count)

    close(fp);
    return EXIT_SUCCESS;
}


/*
 * OS HW4 0110143
 * Common Function Library
 * redfish.tbc@gmail.com
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <common.h>
#include <types.h>

int read_n_bytes(int fp, void *dest, int num_bytes, int offset)
{
    /** OFFSET FROM BEGINING OF PARTITION WHERE THE INFO RESIDES */ 
    // off_t lseek(int fd, off_t offset, int whence);
    if (lseek(fp, offset, SEEK_SET) == -1){
        perror("Seek error");
        return -1;
    }
	
    /** READ BYTES */
    // ssize_t read(int fd, void *buf, size_t count);
    if (read(fp, dest, num_bytes) == -1){
        perror("Read error");
        return -1;
    }

    return 0;
}

int write_n_bytes(int fp, void *buf, int num_bytes, int offset) 
{
    /** OFFSET FROM BEGINING OF PARTITION WHERE THE INFO RESIDES */ 
    // off_t lseek(int fd, off_t offset, int whence);
    if (lseek(fp, offset, SEEK_SET) == -1){
        perror("Seek error");
        return -1;
    }
    
    /** WRITE BYTES */
    // ssize_t write(int fd, const void *buf, size_t count);)
    if (write(fp, buf, num_bytes) == -1){
        perror("Write error");
        return -1;
    }
    
    return 0;
}

void hexDump(void *source, int size, int start_offset) 
{
    int i,j;
    int start = 0 + start_offset;
    
    unsigned char *checker = (unsigned char*)malloc(size);
    memcpy(checker, source, size);
    
    /*************  HEADER  *************/
    printf("address\t");
    for(i = 0; i < 16; i++) printf("%2X ", i);
    
    printf(" ");
    for(i = 0; i < 16; i++) printf("%X", i);
    
    printf("\n");
    for(i = 0; i < size; i++) 
    {
        /********* DATA *********/
        if(i%16 == 0) printf("%07x\t", start+i);
        printf("%02x ", checker[i]);
        
        /*********** REFERENCE VALUES **********/
        if((i+1)%16 == 0) 
        {
            printf("|");
            for(j = i-15; j <= i; j++) 
            {
                if(checker[j] >= 0x20 && checker[j] <= 0x7E)
                    printf("%c", checker[j]);
                else printf(".");
            }
            
            printf("|\n");
        }
    }
    
    printf("\n");
    free(checker);
}

void atoi_array(unsigned char *dest, char **src, int size) 
{
    int i;
    for(i = 0; i < size; i++) {
	dest[i] = atoi(src[i]);
    }
}

/******************************************************************************
 * MY STRUCT INFORMATION OF FILE SYSTEM
 *****************************************************************************/
uint32_t getDRBase(boot_record_t boot){
    uint32_t sectorBase = 0;
    sectorBase = 
        (boot.reserved_region_ssec + boot.sectors_per_FAT * boot.FAT_copies);
    
    printf("Data Rigion Base (sectors) : %d\n", sectorBase);
    return sectorBase;
}

int media_sec_read(int fp, unsigned char *buf, uint32_t begin, uint32_t count)
{
    
    unsigned long i;
    for(i = 0; i < count; i++){
        // size   : 512
        // start  : begin*512
        read_n_bytes(fp, buf, 512, begin*512);
        count ++;
        buf += 512;
    }

    return EXIT_SUCCESS;
}

/******************************************************************************
 * Malware "0110143 ROCKS!"
 *****************************************************************************/
int replace(int fp, const char *str, uint32_t sector_begin, uint32_t limit){
    uint32_t        start       =  sector_begin * 512; // (after root dir)
    uint32_t        limit_b     =  limit * 512;
    uint8_t         strsize     =  strlen(str);    
    uint8_t        *buffer      =  strcast(str);
    unsigned char  *tag         =  (unsigned char*) malloc(8);
    
    printf("string start from bytes dec (%d), hex (%x).\n", start, start);
    printf("data limit in bytes, hex (%x).\n", limit_b);

    while ((start+strsize) < limit_b){
        write_n_bytes(fp, buffer, strsize, start);
        start += strsize;
    }
    
    free (tag);
    free (buffer);
    return EXIT_SUCCESS;
}

// check if block is used or not
int chkUsage(int fp, uint32_t start){
    unsigned char  *buffer;
    buffer = (unsigned char*) malloc(32);
    
    read_n_bytes(fp, buffer, 32, start);
    int i = 0;
    // int count = 0;
    for(i = 0; i < 32; i++){
        if(buffer[i] != 0x00) return 1; // used 
        // else count++;
    }
    return 0; // unuesd
}

// Cast char arr to int arr
uint8_t *strcast(const char *str) {
    int i = 0;
    int size = strlen(str);
    uint8_t *intarr = (uint8_t*) malloc(size);
    
    for (i = 0; i < size; i++)
        intarr[i] = (uint8_t) str[i];
 
    return intarr;
}

